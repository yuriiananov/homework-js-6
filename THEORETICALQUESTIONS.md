1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.

Для того щоб компілятор міг коректно читати код та відображати його вміст використовується екранування. Екранування це застосування символів, які
допомагають програмі зрозуміти, що наприклад в тексті мені потрібно огорнути певне слово в подвійні лапки, а не вказати стрінговий тип даних, або просто поставити в рядку слеш, чи перенести текст на інший рядок і т.д.

2. Які засоби оголошення функцій ви знаєте?

- Оголошення фунції за домомогою ключового слова function (function declaration);
- Оголошення функції без імені, яка записується в змінну (function expression);
- Оголошення функції з іменем, яка записується в змінну (named function expression).

3. Що таке hoisting, як він працює для змінних та функцій?

Коли ми декларуємо функцію (function declaration) або оголошуєм змінну відбувається hoisting (всплиття). Програма перед виконанням коду піднімає задекларовані функції та оголошені змінні на початок області видимості. За допомогою цього ми можемо використовувати змінні та фунції до їх фактичного оголошення в коді.
