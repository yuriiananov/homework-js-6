"use strict";

/*Завдання
Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

  Технічні вимоги:
Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
  1. При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
  2. Створити метод getAge() який повертатиме скільки користувачеві років.
  3. Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з’єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об’єкта.*/

function createNewUser() {
  const firstName = prompt("Введіть своє ім'я:");
  const lastName = prompt("Введіть своє прізвище:");
  const birthday = prompt("Введіть свою дату народження у форматі dd.mm.yyyy:");

  const parts = birthday.split('.');
  const birthdayDay = +parts[0];
  const birthdayMonth = +parts[1] - 1;
  const birthdayYear = +parts[2];

  const newUser = {
    firstName,
    lastName,
    birthday: new Date(birthdayYear, birthdayMonth, birthdayDay),

    getLogin: () => `${firstName[0].toLowerCase()}${lastName.toLowerCase()}`,

    getAge: function() {
      const currentDate = new Date();
      const birthdayDate = this.birthday;
      let age = currentDate.getFullYear() - birthdayDate.getFullYear();
      const birthMonth = currentDate.getMonth() - birthdayDate.getMonth();

      if (birthMonth < 0 || (birthMonth === 0 && currentDate.getDate() < birthdayDate.getDate())) {
        age--;
      }

      return age;
    },

    getPassword: function() {
      const firstLetter = this.firstName[0].toUpperCase();
      const lastNameLower = this.lastName.toLowerCase();
      const year = this.birthday.getFullYear();

      return firstLetter + lastNameLower + year;
    }
  };

  return newUser;
}

const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
